﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Klingsten.Snippet;

namespace Test.Snippet {

    [TestClass]
    public class Test_CreateCSharpClassFromDatabase {

        #region Create C# and TypeScript classes

        [TestMethod]
        public void Snippet_Create_CSharpClassFromDatabase_CreateCSharpClass() {
            // arrange 

            // access
            DapperHelper.CreateCSharpClassFromDatabase("AccountNumber");
            DapperHelper.CreateCSharpClassFromDatabase("Company");
            DapperHelper.CreateCSharpClassFromDatabase("Contact");
            DapperHelper.CreateCSharpClassFromDatabase("ContactGroup");
            DapperHelper.CreateCSharpClassFromDatabase("ContactType");
            DapperHelper.CreateCSharpClassFromDatabase("Invoice");
            DapperHelper.CreateCSharpClassFromDatabase("Log");

        }

        [TestMethod]
        public void Snippet_Create_TypeScript_ClassFromDatabase_CreateTypeScriptClass() {
            // arrange 

            // access
            DapperHelper.CreateTypeScriptIntefaceFromDatabase("AccountNumber");
            DapperHelper.CreateTypeScriptIntefaceFromDatabase("AccountEntry");

            DapperHelper.CreateTypeScriptIntefaceFromDatabase("Company");
            DapperHelper.CreateTypeScriptIntefaceFromDatabase("Contact");
            DapperHelper.CreateTypeScriptIntefaceFromDatabase("ContactGroup");
            DapperHelper.CreateTypeScriptIntefaceFromDatabase("ContactType");
            DapperHelper.CreateTypeScriptIntefaceFromDatabase("Invoice");
            DapperHelper.CreateTypeScriptIntefaceFromDatabase("Log");
        }
        #endregion 


    }
}
