﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Klingsten.Snippet {
    public class Regex {

        private static System.Text.RegularExpressions.Regex emailRegex = new System.Text.RegularExpressions.Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
        private static System.Text.RegularExpressions.Regex emailDomainRegex = new System.Text.RegularExpressions.Regex(@"@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
        private static System.Text.RegularExpressions.Regex nameFromEmailRegex = new System.Text.RegularExpressions.Regex(@"\w+([-+.]\w+)*@*", RegexOptions.IgnoreCase);
        
        public static string RemoveIllegalCharsFromFileName(string str) {
            return System.Text.RegularExpressions.Regex.Replace(str, "[^a-zA-Z0-9æøåÆÅØ_ -.]+", "_", RegexOptions.Compiled);
        }
        
        #region Files

        public static List<string> GetEmailsList(string data) {
            MatchCollection emailMatches = emailRegex.Matches(data);
            var emails = new List<string>();
            foreach (Match emailMatch in emailMatches) {
                emails.Add(emailMatch.Value.ToLower());
            }
            return emails;
        }

        #endregion files

        #region Emails

        public static string GetDomainFromEmailAdr(string data) {
            MatchCollection emailMatches = emailDomainRegex.Matches(data);
            try {
                return emailMatches[0].Value;
            }
            catch {
                return string.Empty;
            }
        }

        public static bool IsValidEmail(string email) {
            if (email == null || email.Length < 3) return false;
            MatchCollection emailMatches = emailRegex.Matches(email);
            return emailMatches.Count == 1 ? true : false;
        }

        public static string GetNameFromEmailAdr(string data) {
            MatchCollection emailMatches = nameFromEmailRegex.Matches(data);
            try {
                return emailMatches[0].Value;
            }
            catch {
                return string.Empty;
            }
        }

        #endregion emails

    }
}