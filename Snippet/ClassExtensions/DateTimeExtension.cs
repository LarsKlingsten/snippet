﻿using System;

namespace Klingsten.ClassExtensions {

    public static class DateTimeExtensions {

        private const string SHORT_DATE = "dd.MM.yy";
        private const string SQL_DATE = "yyyy-MM-dd HH:mm";

        public static string NowFormats(this DateTime datetime) {
            if (datetime == null) { return string.Empty; }
            if (datetime.DayOfYear == DateTime.Now.DayOfYear && datetime.Year == DateTime.Now.Year) {
                return datetime.FullHour();
            }
            return datetime.ToString(datetime.FullDate());
        }

        public static string FullDate(this DateTime datetime) {
            if (datetime == null) { return string.Empty; }
            return datetime.ToString(SHORT_DATE);
        }

        public static string FullHour(this DateTime datetime) {
            if (datetime == null) { return string.Empty; }
            return datetime.ToString(SHORT_DATE);
        }


        public static string FullDateHour(this DateTime datetime) {
            if (datetime == null) { return string.Empty; }
            return datetime.ToString("dd.MM.yy HH:mm");
        }

        public static string SqlDateTime(this DateTime datetime) {
            if (datetime == null) { return string.Empty; }
            return datetime.ToString(SQL_DATE);
        }
    }
}