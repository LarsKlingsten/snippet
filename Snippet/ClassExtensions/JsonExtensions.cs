﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Klingsten.ClassExtensions {
    public static class JsonExtensions {

        private static JsonSerializerSettings JSON_IGNORENULL = new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore };
        public static JObject ToJObjectIgnoreNull(this object o) {
            return JObject.Parse(JsonConvert.SerializeObject(o, JSON_IGNORENULL));
        }
        public static JObject ToJObject(this object o) {
            return JObject.Parse(JsonConvert.SerializeObject(o));
        }
    }
}