﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Klingsten.ClassExtensions {
    
    public static class DynamicQuery {
        //   usage like this ->   IQueryable q = someData.OrderByDynamic("id", true);
        // Dynamically creates a call like this: query.OrderBy(p => p.SortColumn)
        public static IQueryable<T> OrderByDynamic<T>(this IQueryable<T> query, string sortColumn, bool descending) {
            var parameter = Expression.Parameter(typeof(T), "p");

            string orderbyString = descending ? "OrderBy" : "OrderByDescending";
            var property = typeof(T).GetProperty(sortColumn);
            var propertyAccess = Expression.MakeMemberAccess(parameter, property); // this is the part p.SortColumn
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);  // this is the part p => p.SortColumn;

            // finally, call the "OrderBy" / "OrderByDescending" method with the order by lamba expression
            Expression resultExpression = Expression.Call(typeof(Queryable), orderbyString, new Type[] { typeof(T), property.PropertyType },
               query.Expression, Expression.Quote(orderByExpression));
            return query.Provider.CreateQuery<T>(resultExpression);
        }
    }
}