﻿using System;
using System.Diagnostics;
using System.Linq;

namespace Klingsten.ClassExtensions {

    public static class StringExtensions {
        /// <summary>
        /// Note that the methods returns 'string.Empty' also for null values
        /// which appears practical for DB usage when null values are not allowed.
        /// </summary>
        public static string Left(this string str, int length) {
            if (str == null || length <= 0) { return string.Empty; }
            if (length > str.Length) { length = str.Length; }
            return str.Substring(0, length);
        }

        public static string LeftWithPostSpaces(this string str, int length) {
            if (str == null || length <= 0) { return string.Empty; }
            if (str.Length < length) { str = str.PadRight(length); }
            return str.Substring(0, length);
        }

        /// <summary>
        /// Note that the methods returns 'string.Empty' also for null values
        /// which appears practical for DB usage when null values are not allow.
        /// </summary>
        public static string Right(this string str, int length) {
            if (str == null || length <= 0) { return string.Empty; }
            if (length > str.Length) { length = str.Length; }
            return str.Substring(str.Length - length, length);
        }

        public static string RemoveDigits(this string str) {
            return new string(str.Where(char.IsLetter).ToArray());
        }
        public static string RemoveNonDigits(this string str) {
            return new string(str.Where(char.IsDigit).ToArray());
        }

        /// <summary>
        /// NOT tested - ever
        /// </summary>
        public static string RemoveCharFromString(this string str, char removeChar) {
            char[] RemoveTheseChars = { removeChar };
            return str.Trim(RemoveTheseChars);
        }


        /// <summary>
        /// Changes to first letter in string to either UpperCase or LowerCase 
        /// </summary>
        public static string ChangeCaseforFirstChar(this string input, bool toUpperCase = true) {
            char[] array = input.ToCharArray();
            array[0] = toUpperCase ? char.ToUpperInvariant(array[0]) : array[0] = char.ToLowerInvariant(array[0]); // 
            return new string(array);
        }
        
        #region debug/console

        // console
        public static void ToConsole(this string str) {
            Console.WriteLine(str);
        }

        public static void ToConsoleWithTime(this string str) {
            Console.WriteLine(str.Time());
        }
        public static void ToConsoleWithTimeUtc(this string str) {
            Console.WriteLine(str.TimeUtc());
        }
        public static void ToConsoleWithDateTime(this string str) {
            Console.WriteLine(str.DateTime());
        }
        public static void ToConsoleWithDateTimeUtc(this string str) {
            Console.WriteLine(str.DateTimeUtc());
        }

        // debug
        public static void ToDebugWithTime(this string str) {
            Debug.WriteLine(str.Time());
        }
        public static void ToDebugWithDateTimeUtc(this string str) {
            Debug.WriteLine(str.DateTimeUtc());
        }

        // string
        public static string Time(this string str) {
            return (string.Format("{0} {1}", System.DateTime.Now.ToString("HH:mm:ss.fff"), str));
        }

        public static string TimeUtc(this string str) {
            return (string.Format("{0} {1}", System.DateTime.UtcNow.ToString("HH:mm:ss.fff"), str));
        }

        public static string DateTime(this string str) {
            return (string.Format("{0} {1}", System.DateTime.Now.ToString("yyyy-MM.dd HH:mm:ss"), str));
        }
        public static string DateTimeUtc(this string str) {
            return (string.Format("{0} {1}", System.DateTime.UtcNow.ToString("yyyy-MM.dd HH:mm:ss"), str));
        }

        // decimals
        public static decimal TimerTickToMilliSeconds(this long ticks) {
            return decimal.Divide(ticks * 1000, Stopwatch.Frequency);
        }

        #endregion
    }
}
