﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Klingsten.Snippet {
    public class MyException : Exception {

        public string FailedClass { get; set; }
        public string FailedMethod { get; set; }
        public string MessageToUser { get; set; }
        public string MessageToProgrammer { get; set; }
        public int ErrorNumber { get; set; }
        public Exception Exception { get; set; }

        /// <summary>
        /// Provides standard exceptions, and automatically provides where the exception incurred. If ErrorNumber is initilized to -1;
        /// </summary>

        public MyException([CallerMemberName]string method = "") {
            FailedMethod = method;
            StackTrace stackTrace = new StackTrace(1, false);   // http://stackoverflow.com/questions/17893827/c-sharp-how-to-get-type-name-of-a-callermember
            FailedClass = stackTrace.GetFrame(1).GetMethod().DeclaringType.FullName;

            ErrorNumber = -1; // not set
            MessageToUser = "No details for failure provided";
            MessageToProgrammer = "No details provided";


            Debug.WriteLine(ToString());
        }

        public override string ToString() {
            return $"user={MessageToUser} prg={MessageToProgrammer} class={FailedClass}  method={ FailedMethod} exception={Exception?.Message} ";
        }

        public static string GetCurrentMethodName([CallerMemberName]string method = "") {
            return method;
        }
    }
}
