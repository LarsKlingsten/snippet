﻿// Lars Klingsten 2016-05-11
using System;
using System.Collections.Generic;

namespace Klingsten.Snippet {

    public class DictWithLists<K, V> {

        private Dictionary<K, IEnumerable<V>> _dict;
        private Dictionary<K, DateTime> _lastWrite;
        private Dictionary<K, DateTime> _lastRead;
        private int _CacheExpiresMinutes;

        public DictWithLists(int cacheExpiresMinutes = 0) {
            _dict = new Dictionary<K, IEnumerable<V>>();
            _lastWrite = new Dictionary<K, DateTime>();
            _lastRead = new Dictionary<K, DateTime>();
            _CacheExpiresMinutes = cacheExpiresMinutes;

            if (cacheExpiresMinutes < 0) { throw new ArgumentException("Bad parameter: CacheExpiresTime can not be negative integer"); }
        }

        /// <summary>
        ///// Replace All Values for Key
        /// </summary>
        public void SetList(K key, IEnumerable<V> newValues) {

            IEnumerable<V> values = null;
            if (!_dict.TryGetValue(key, out values)) {
                _dict.Add(key, newValues);
            }
            else {
                _dict[key] = newValues;
            }
            SetLastWrite(key);
            SetLastRead(key);
        }

        public IEnumerable<V> GetItems(K key) {
            IEnumerable<V> value = null;
            _dict.TryGetValue(key, out value);
            SetLastRead(key);
            return value;
        }
        public List<V> GetItemsAsList(K key) {
            return GetItems(key) as List<V>;
        }

        public V[] GetItemsAsArray(K key) {
            return GetItems(key) as V[];
        }

        public int GetKeysCount() {
            return _dict.Count;
        }

        public bool isKeyExist(K key) {
            return GetItems(key) != null;
        }

        /// <summary>
        /// return true, also if key does not exist
        /// </summary>
        public bool isCacheExpired(K key) {
            if (_CacheExpiresMinutes == 0) { return false; }
            DateTime cacheExpiresTime = GetLastWrite(key).AddMinutes(_CacheExpiresMinutes);
            return MyTimer.Now().Ticks - cacheExpiresTime.Ticks > 0;
        }

        /// <summary>
        /// Removed all item for a specific key (incl last read/Last write for key)
        /// </summary>
        public bool RemoveItems(K key) {
            _lastRead.Remove(key);
            _lastWrite.Remove(key);
            return _dict.Remove(key);
        }

        public override string ToString() {
            return $"ListsCount={_dict.Count} Key={typeof(K)} Values=List<{typeof(V)}>";
        }

        #region Set/Get DateTime for possible Caching
        private void SetLastWrite(K key) {
            _lastWrite[key] = MyTimer.Now();
        }

        private void SetLastRead(K key) {
            _lastRead[key] = MyTimer.Now();
        }

        /// <summary>
        /// Return Minimum DateTime if Key was not found
        /// </summary>
        public DateTime GetLastRead(K key) {
            DateTime value;
            _lastRead.TryGetValue(key, out value);
            return value;
        }

        /// <summary>
        /// Return Minimum DateTime if Key was not found i.e. return default(DateTime)
        /// </summary>
        public DateTime GetLastWrite(K key) {
            DateTime value;
            _lastWrite.TryGetValue(key, out value);
            return value;
        }


        #endregion
    }
}
