﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Klingsten.Snippet;
using Klingsten.ClassExtensions;
using System;

namespace Test.Snippet {

    [TestClass]
    public class Test_DictWithLists {

        #region  Snippet_Constructor()

        [TestMethod]
        public void Snippet_DictWithLists_GetValues() {
            // arrange
            var dict = new DictWithLists<int, double>();

            double[] doubleArray = new double[] { 1.1, 1.2, 1.3 };

            // Act
            dict.SetList(1, doubleArray);
            double[] result1 = dict.GetItemsAsArray(1);

            // Assert
            Assert.IsNotNull(result1);
            Assert.AreEqual(1.1, result1[0]);
            Assert.AreEqual(1.3, result1[2]);
        }

        [TestMethod]
        public void Snippet_DictWithLists_AddMulti_Get() {
            // arrange
            IList<double> doubles = new List<double>() { 1.1, 1.2, 1.3 };
            var dict = new DictWithLists<int, double>();
            dict.SetList(1, doubles);

            // Act

            IList<double> result1 = dict.GetItemsAsList(1);

            // Assert
            Assert.AreEqual(1.1, result1[0]);
            Assert.AreEqual(1.2, result1[1]);
            Assert.AreEqual(1, dict.GetKeysCount());


            MyLog.Log($"Key=1 {dict.GetLastRead(1)} last read");
            MyLog.Log($"{dict.ToString()}");
        }

        [TestMethod]
        public void Snippet_DictWithLists_Remove() {
            // arrange
            List<double> doubles = new List<double>() { 1.1, 1.2, 1.3 };
            var dict = new DictWithLists<int, double>();

            // act
            dict.SetList(1, doubles);

            //Assert  
            Assert.AreEqual(1, dict.GetKeysCount());

            // Act
            dict.RemoveItems(1);
            Assert.AreEqual(0, dict.GetKeysCount());

            //Asssrt
            MyLog.Log($"Key=1 {dict.GetLastRead(1)} last read");

            MyLog.Log($"{dict.ToString()}");
        }

        [TestMethod]
        public void Snippet_DictWithLists_IsKeyExist() {
            // arrange
            List<double> doubles = new List<double>() { 1.1, 1.2, 1.3 };

            const string myKey = "MyKey";

            // act
            var dict = new DictWithLists<string, double>();
            Assert.AreEqual(0, dict.GetKeysCount());

            Assert.IsFalse(dict.isKeyExist("Does Not Exist"));

            dict.SetList(myKey, doubles);

            //Assert (key is added)  
            Assert.IsFalse(dict.isKeyExist("Does Not Exist"));
            Assert.IsTrue(dict.isKeyExist(myKey));
            Assert.AreEqual(1, dict.GetKeysCount());

            // Act
            dict.RemoveItems(myKey);

            //Assert (key is delete)
            Assert.AreEqual(0, dict.GetKeysCount());
            Assert.IsFalse(dict.isKeyExist("Does Not Exist"));
            Assert.IsFalse(dict.isKeyExist(myKey));
        }


        [TestMethod]
        public void Snippet_DictWithLists_NotFound() {
            // arrange
            List<double> doubles = new List<double>() { 1.1, 1.2, 1.3 };
            var dict = new DictWithLists<string, double>();

            // Act
            dict.SetList("myKey", doubles);

            // Assert
            Assert.IsNotNull(dict.GetLastRead("This Key Does Not Exist"));
            Assert.AreEqual(false, dict.RemoveItems("This Key Does Not Exist"));
            Assert.AreEqual(true, dict.RemoveItems("myKey")); // this key does (did exist)
            var NotExist = dict.GetItems("Does Not Exist");

            MyLog.Log($"{dict.ToString()}");
        }

        [TestMethod]
        public void Snippet_DictWithLists() {
            // arrange
            List<double> doubles = new List<double>() { 1.1, 1.2, 1.3 };
            var dict = new DictWithLists<string, double>();

            // Act
            dict.SetList("myKey", doubles);

            // Assert
            Assert.IsNotNull(dict.GetLastRead("This Key Does Not Exist"));
            Assert.AreEqual(false, dict.RemoveItems("This Key Does Not Exist"));
            Assert.AreEqual(true, dict.RemoveItems("myKey")); // this key does (did exist)
            var NotExist = dict.GetItems("Does Not Exist");

            MyLog.Log($"{dict.ToString()}");
        }
        #endregion

        #region Cache
        [TestMethod]
        public void Snippet_DictWithLists_Cache_DoesNotExpire() {
            // arrange
            var dict = new DictWithLists<string, double>(0); // 0 == timeout in Minutes

            const string MY_KEY = "myKey";

            // Act
            dict.SetList(MY_KEY, new List<double>() { 1.1, 1.2, 1.3 });

            // Assert
            Assert.IsFalse(dict.isCacheExpired(MY_KEY));

        }


        [TestMethod]
        public void Snippet_DictWithLists_Cache_NotExpired() {
            // arrange
            var dict = new DictWithLists<string, double>(1); // 1 == timeout in Minutes
            const string MY_KEY = "myKey";

            // Act
            dict.SetList(MY_KEY, new List<double>() { 1.1, 1.2, 1.3 });

            // Assert
            Assert.IsFalse(dict.isCacheExpired(MY_KEY));
        }

        //  [TestMethod] // time out is difficult to test
        public void Snippet_DictWithLists_Cache_Expired() {
            // arrange
            var dict = new DictWithLists<string, double>(1); // 1 == timeout in Minutes
            const string MY_KEY = "myKey";

            // Act
            dict.SetList(MY_KEY, new List<double>() { 1.1, 1.2, 1.3 });
            System.Threading.Thread.Sleep(500);
            Assert.IsFalse(dict.isCacheExpired(MY_KEY));

            // Assert
            System.Threading.Thread.Sleep(600);
            Assert.IsTrue(dict.isCacheExpired(MY_KEY));
        }


        [TestMethod]
        public void Snippet_DictWithLists_Cache_Without_KEY() {
            // arrange
            var dict = new DictWithLists<string, double>(1); // 1 == timeout in Minutes
            const string MY_KEY = "myKey";

            // Act
            dict.SetList(MY_KEY, new List<double>() { 1.1, 1.2, 1.3 });
            Assert.IsTrue(dict.isCacheExpired("Key does not exist"));
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Snippet_DictWithLists_Cache_badParameter() {
            // arrange
            // act
            var dict = new DictWithLists<string, double>(-1); //  

            //assert -> expect an exception

        }

        #endregion   



    }
}
