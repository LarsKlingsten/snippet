﻿using Klingsten.ClassExtensions;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using System;

namespace Klingsten.Snippet {

    public static class DapperHelper {

        public const string SQL_LOGIN = @"Server=sqlvm\sqlexpress;Database=accountdb;User Id=sa; Password = larslusk001; ";

        private static Dictionary<string, string> DatabaseToCSharpMapping = new Dictionary<string, string>() {
                { "nvarchar", "string" },
                { "datetime", "DateTime" },
                { "bit", "bool" },
                { "int", "int" },
                { "decimal", "decimal" },
                { "smalldatetime", "DateTime" },
                { "image", "object" },
                { "money", "decimal" },
                { "ntext", "string" },
                { "nchar", "string" },
            };

        private static Dictionary<string, string> DatabaseToTypeScriptMapping = new Dictionary<string, string>() {
                { "nvarchar", "string" },
                { "datetime", "Date" },
                { "bit", "boolean" },
                { "int", "number" },
                { "decimal", "number" },
                { "smalldatetime", "Date" },
                { "image", "object" },
                { "money", "number" },
                { "ntext", "string" },
                { "nchar", "string" },
            };

        public static void CreateCSharpClassFromDatabase(string databaseTable) {

            using (SqlConnection cn = new SqlConnection(SQL_LOGIN)) {

                string sql = $" select  * from {databaseTable}";
                IDataReader dr = cn.ExecuteReader(sql);
                DataTable schemaTable = dr.GetSchemaTable();
                $" public class {databaseTable}  {{".ToConsole();
                string objectType = "";

                foreach (DataRow myField in schemaTable.Rows) {
                    objectType = myField[24].ToString();
                    try {
                        $" public {  DatabaseToCSharpMapping[objectType] }  { myField[0]}  {{ get; set; }} ".ToConsole();
                    }
                    catch {
                        $"Your DB to Object Does not exist - addit {objectType }".ToString().ToConsole();
                    }
                }

                $"}}".ToConsole();
            }
        }
        
        public static void CreateTypeScriptIntefaceFromDatabase(string databaseTable) {

            using (SqlConnection cn = new SqlConnection(SQL_LOGIN)) {

                string sql = $" select  * from {databaseTable}";
                IDataReader dr = cn.ExecuteReader(sql);
                DataTable schemaTable = dr.GetSchemaTable();
                $" export interface {databaseTable}  {{".ToConsole();
                string objectType = "";

                foreach (DataRow column in schemaTable.Rows) {
                    objectType = column[24].ToString();
                    try {
                        $"    {column[0]} : {DatabaseToTypeScriptMapping[objectType]};   ".ToConsole();
                    }
                    catch {
                        $"Your DB to Object Does not exist - addit {objectType }".ToString().ToConsole();
                    }
                }

                $"}}".ToConsole();
            }
        }

    }
}