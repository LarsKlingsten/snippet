﻿using System;
using System.Globalization;

namespace Klingsten.Snippet {

    public enum CountryCode {
        DK = 45,
        US = 1
    }

    public class Parse {

        #region Parse Decimal

        private static NumberFormatInfo numberFormatDK = new NumberFormatInfo { NumberDecimalSeparator = ",", NumberGroupSeparator = ".", NumberDecimalDigits = 2 };
        private static NumberFormatInfo numberFormatUS = new NumberFormatInfo { NumberDecimalSeparator = ".", NumberGroupSeparator = ",", NumberDecimalDigits = 2 };

        public static decimal? TryParseStringToDecimal(string inputString, CountryCode countryCode = CountryCode.DK) {

            decimal decimalValue;

            NumberFormatInfo numberFormat;

            if (countryCode == CountryCode.DK) {
                numberFormat = numberFormatDK;
            }
            else if (countryCode == CountryCode.US) {
                numberFormat = numberFormatUS;
            }
            else {
                throw new InvalidOperationException($"no format exist for {countryCode}");
            }

            // remove thousand "," or "." seperators
            string decimalStringCleaned = inputString.Replace(numberFormat.NumberGroupSeparator, string.Empty); ;
            NumberStyles style = NumberStyles.Number | NumberStyles.AllowCurrencySymbol;


            // allow to return a null value instead of default zero
            if (decimal.TryParse(decimalStringCleaned, style, numberFormat, out decimalValue)) {
                return decimalValue; // parsing was ok
            };

            return null;      // parsing failed
        }
        #endregion Parse Double

        #region parse DateTime
        public static DateTime? TryParseStringToDateTime(string dateString, string DateFormat = "yyyy-MM-dd") {
            DateTime tempValue;

            if (DateTime.TryParseExact(dateString.Trim(), DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out tempValue)) {
                return tempValue; // parsing ok
            }

            return null;    // parsing failed
        }
        #endregion parse DateTime

    }
}