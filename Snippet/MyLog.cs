﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Klingsten.Snippet {
    public class MyLog {

        const string LONGTIMEFORMAT = "MM.dd HH:mm:ss.fff";

        public static string GetCurrentMethodName([CallerMemberName]string method = "") {
            return method;
        }

        static public void Log(string msg, [CallerMemberName]string method = "") {
            string returnMessage = $"{NowToString()} {method}: {msg}";

            Console.WriteLine(returnMessage);
            Debug.WriteLine(returnMessage);
        }

        static public void LogDebug(string msg) {
            Debug.WriteLine($"{NowToString()} {msg}");
        }

        static public void LogPanic(string msg) {
            Console.WriteLine($"{NowToString()} Panic {msg}");
            Debug.WriteLine($"{NowToString()} {msg}");
        }

        /// <summary>
        /// Method will is likely to be removed shortly. Use Klingsten.Snippet.MyTimer.Now() instead
        /// </summary>
        [Obsolete]
        public static DateTime Now() {
            return MyTimer.Now();
        }

        public static string NowToString() {
            return MyTimer.Now().ToString(LONGTIMEFORMAT);
        }

        static public void LogSqlException(SqlException e) {
            Log($"{e.Message}");
        }
    }
}
