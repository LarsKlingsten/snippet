﻿using System.Threading;

namespace Klingsten.Snippet {
    public static class SystemInfo {

        public static string GetThreadInfo() {
            var t = Thread.CurrentThread;
            return $"ThreadInfo: IsBackGround={t.IsBackground} isPool={t.IsThreadPoolThread}";
        }
    }
}
