﻿using System.Diagnostics;
using System.IO;
using System.Text;

namespace Klingsten.Snippet {

    public static class Files {

        public static string ReadFile(string path, Encoding encoding = null) {

            
            if (encoding == null) { encoding = Encoding.GetEncoding("iso-8859-1"); } // try eventually Encoding.UTF8 };

            try {
                StreamReader sr = new StreamReader(path, Encoding.GetEncoding("iso-8859-1"), true);
                StringBuilder lines = new StringBuilder();
                while (sr.Peek() >= 0) {
                    string line = sr.ReadLine();
                    lines.AppendLine(line);
                }
                sr.Close();
                return lines.ToString();
            }
            catch (IOException e) {
                throw new System.Exception($"Reading file '{path}' failed", e);
            }
        }

        public static bool DeleteFile(string pathAndFileName) {
            bool isSuccess = true;
            try {
                File.Delete(pathAndFileName);
            }
            catch (IOException e) {
                Debug.WriteLine($"DeleteFile '{pathAndFileName}' failed cause={e.Message}");
                isSuccess = false;
            }
            return isSuccess;
        }

        public static bool DeleteFolder(string path) {
            bool isSuccess = true;
            try {
                Directory.Delete(path);
            }
            catch (IOException e) {
                Debug.WriteLine($"DeleteFile '{path}' failed cause={e.Message}");
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}