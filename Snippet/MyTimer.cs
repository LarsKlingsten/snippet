﻿using System;

namespace Klingsten.Snippet {

    public class MyTimer {

        /// <summary>
        /// Returns UTC DateTime
        /// </summary>
        public static DateTime Now() {
            return DateTime.UtcNow;
        }

        DateTime _startTime;

        public MyTimer() {
            _startTime = DateTime.Now;
        }

        public double ElapsedMilliSec() {
            return (DateTime.Now.Subtract(_startTime)).TotalMilliseconds / 1000;
        }

        public void Reset() {
            _startTime = DateTime.Now;
        }

        public String ElapsedString() {
            return string.Format("in {0} secs", ElapsedMilliSec());
        }

    }

}
