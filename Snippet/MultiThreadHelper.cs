﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Klingsten.Snippet {

    public class MultiThreadHelper {
        private int _threadCount;
        private Object myLock = new Object(); // Lock object - to ensure concurrency

        public void SetThreadCount(int count) {
            _threadCount = count;
        }

        // Decreased the Count (it will reach 0)
        public void ThreadDecreaseCount() {
            lock (myLock) {
                _threadCount--;
            }
        }

        /// <summary>
        /// Loop that awaits that all threads have been completed
        /// </summary>
        public void AwaitMultiThreadCompletion() {
            while (_threadCount != 0) {
                Thread.Sleep(100);
                Console.Write(".");
            }
            Console.WriteLine(string.Empty);
        }

        /// <summary>
        /// Usage Sample (its untested)
        /// </summary>

        public void SetupThreadPoolAndCallProcess() {
            int MaxThreadsInThreadpool = 100;
            int NumberOfTotalThreads = 1000;

            SetThreadCount(NumberOfTotalThreads);   // set the max value (which will decreased on each call to  // SomeSlowProcessThatIsBeingRunOnItsOwnThread

            ServicePointManager.DefaultConnectionLimit = MaxThreadsInThreadpool;
            ThreadPool.SetMaxThreads(MaxThreadsInThreadpool, MaxThreadsInThreadpool);

            for (int i = 0; i < NumberOfTotalThreads; i++) {
                Task.Run(() => SomeSlowProcessThatIsBeingRunOnItsOwnThread("Call#" + i)); // call using the threadpool
            }
            AwaitMultiThreadCompletion();
        }

        public string SomeSlowProcessThatIsBeingRunOnItsOwnThread(string someString) {
            ThreadDecreaseCount(); // decreased the number of threads (to stop the process until threads are all completed)

            for (int i = 0; i < 3; i++) {      // simulates a slow call like HttpRequest or DB calls (or whatever)
                Thread.Sleep(1000);

            }
            Console.WriteLine("Completed #" + someString);
            return "done#" + someString;
        }



    }
}
