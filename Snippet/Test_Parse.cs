﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Klingsten.Snippet;
using Klingsten.ClassExtensions;
using System;

namespace Test.Snippet {

    [TestClass]
    public class Test_Parse {

        #region  Snippet_TryParseStringToDouble()

        [TestMethod]
        public void Snippet_TryParseStringToDecimal() {
            // arrange 
            Dictionary<string, decimal?> parseDK = new Dictionary<string, decimal?>() {
                  { "123",     (decimal) 123},
                  { "123,4",   (decimal) 123.4},
                  { "123,45", (decimal)  123.45},
                  { "123,46",  (decimal) 123.46},
                  { "123,47", (decimal)  123.47},
                  { "1.234,50",(decimal) 1234.5},
                  { "1.234",   (decimal) 1234},
                  { "Not a Number",   null},

            };

            Dictionary<string, decimal?> parseUS = new Dictionary<string, decimal?>() {
                  { "223",     (decimal) 223},
                  { "2234.5",  (decimal) 2234.5},
                  { "223.4",   (decimal) 223.4},
                  { "223.45",  (decimal)  223.45},
                  { "223.47",  (decimal) 223.47},
                  { "2,234.5", (decimal)  2234.5},
                  { "2,234",   (decimal) 2234}
            };

            // access
            // assert
            foreach (var s in parseDK) {
                decimal? result = Parse.TryParseStringToDecimal(s.Key, CountryCode.DK);
                $"{result} = {s.Value} ({s.Key})     OK={s.Value == result }".ToConsoleWithDateTime();
                Assert.AreEqual(s.Value, result);
            }

            foreach (var s in parseUS) {
                decimal? result = Parse.TryParseStringToDecimal(s.Key, CountryCode.US);
                $"{result} = {s.Value} ({s.Key})     OK={s.Value == result }".ToConsoleWithDateTime();
                Assert.AreEqual(s.Value, result);
            }
        }

        #endregion  Snippet_TryParseStringToDouble()

        #region  Snippet_TryParseStringToDouble()

        [TestMethod]
        public void Snippet_TryParseStringToDate() {
            // arrange 
            Dictionary<string, DateTime?> parse_yyyyMMdd = new Dictionary<string, DateTime?>() {
                  { "2012-01-01",   new DateTime(2012,1,1)   },
                  { " 2012-01-01",  new DateTime(2012,1,1)   },
                  { "2012-01-01 ",  new DateTime(2012,1,1)   },
                  { " 2012-01-01 ", new DateTime(2012,1,1)   },
                  { "2012.01.01",   null   }, // dots instead of minus 
                  { "2012-02-31",   null   }, // bad Date
                  { "BadDate",      null   },
            };

            Dictionary<string, DateTime?> parseDatePlusHours = new Dictionary<string, DateTime?>() {
                  { "2012-01-01 15:23",  new DateTime(2012,1,1).AddHours(15).AddMinutes(23)    },
                  { "2012.01.01 15:93",  null  },
                  { "BadDate",        null  },
            };

            // assert
            foreach (var s in parse_yyyyMMdd) {
                DateTime? result = Parse.TryParseStringToDateTime(s.Key, "yyyy-MM-dd");
                $"Input={s.Key} expected={s.Value} actual={result}  is OK={s.Value == result }".ToConsoleWithDateTime();
                Assert.AreEqual(s.Value, result);
            }

            foreach (var s in parseDatePlusHours) {
                DateTime? result = Parse.TryParseStringToDateTime(s.Key, "yyyy-MM-dd HH:mm");
                $"Input={s.Key} expected={s.Value} actual={result}  is OK={s.Value == result }".ToConsoleWithDateTime();
                Assert.AreEqual(s.Value, result);
            }

        }
        #endregion

    

    }
}
